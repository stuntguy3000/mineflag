package net.stuntguy3000.manager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.stuntguy3000.MFPlugin;
import net.stuntguy3000.enums.LogType;
import net.stuntguy3000.enums.Team;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class mgr_Arena {
	
	public MFPlugin plugin;
	
	public HashMap<String, Team> toBreak = new HashMap<String, Team>();
	public HashMap<String, String> toBreakArena = new HashMap<String, String>();
	
	public mgr_Arena (MFPlugin instance)
	{
		this.plugin = instance;
	}
	
	public Location getSpawn(Team team, String arena)
	{
		File af = new File(plugin.getDataFolder() + File.separator + "arenas.yml");
		YamlConfiguration ac = YamlConfiguration.loadConfiguration(af);
		
		if (team == null || arena == null || arena == "")
		{
			plugin.util.log(LogType.Warning, "[NULL-LOC] Team: " + team + " | Arena: " + arena);
			return null;
		}
		
		if (ac.getString(arena + ".Spawns." + team.name() + ".world") == null)
		{
			return null;
		}
		
		Location spawn = new Location(null, 1, 1, 1);
		
		spawn.setX(ac.getInt(arena + ".Spawns." + team.name() + ".x"));
		spawn.setY(ac.getInt(arena + ".Spawns." + team.name() + ".y"));
		spawn.setZ(ac.getInt(arena + ".Spawns." + team.name() + ".z"));
		spawn.setYaw(ac.getInt(arena + ".Spawns." + team.name() + ".yaw"));
		spawn.setPitch(ac.getInt(arena + ".Spawns." + team.name() + ".pitch"));
		spawn.setWorld(Bukkit.getWorld(ac.getString(arena + ".Spawns." + team.name() + ".world")));
		
		return spawn;
	}
	
	public void setSpawn(Team team, String arena, Location spawn)
	{
		File af = new File(plugin.getDataFolder() + File.separator + "arenas.yml");
		YamlConfiguration ac = YamlConfiguration.loadConfiguration(af);
		
		if (team == null || arena == null || arena == "" || spawn == null)
		{
			plugin.util.log(LogType.Warning, "[SET-LOC] Team: " + team + " | Arena: " + arena);
			plugin.util.log(LogType.Warning, "[SET-LOC] " + spawn.toString());
			
			return;
		}
		
		try {
			ac.set(arena + ".Spawns." + team.name() + ".x", (int) spawn.getX());
			ac.set(arena + ".Spawns." + team.name() + ".y", (int) spawn.getY());
			ac.set(arena + ".Spawns." + team.name() + ".z", (int) spawn.getZ());
			ac.set(arena + ".Spawns." + team.name() + ".yaw", (int) spawn.getYaw());
			ac.set(arena + ".Spawns." + team.name() + ".pitch", (int) spawn.getPitch());
			ac.set(arena + ".Spawns." + team.name() + ".world", spawn.getWorld().getName());
			ac.save(af);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean exists(String arena)
	{
		File af = new File(plugin.getDataFolder() + File.separator + "arenas.yml");
		YamlConfiguration ac = YamlConfiguration.loadConfiguration(af);
		
		return ac.contains(arena);
	}
	
	public List<String> getArenaList()
	{
		File af = new File(plugin.getDataFolder() + File.separator + "arenas.yml");
		YamlConfiguration ac = YamlConfiguration.loadConfiguration(af);
		
		List<String> r = new ArrayList<String>();
		
		for (String name : ac.getKeys(false))
		{
			r.add(name);
		}
		
		return r;
	}
	
	public void createArena(String arena)
	{
		File af = new File(plugin.getDataFolder() + File.separator + "arenas.yml");
		YamlConfiguration ac = YamlConfiguration.loadConfiguration(af);
		
		try {
			ac.set(arena + ".LockedTime.Enabled", false);
			ac.set(arena + ".LockedTime.Time", 10000);
			ac.set(arena + ".MaxPlayers", 50);
			ac.save(af);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void removeArena(String arena)
	{
		File af = new File(plugin.getDataFolder() + File.separator + "arenas.yml");
		YamlConfiguration ac = YamlConfiguration.loadConfiguration(af);
		
		try {
			ac.set(arena, null);
			ac.save(af);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int getMaxPlayerCount(String arena)
	{
		File af = new File(plugin.getDataFolder() + File.separator + "arenas.yml");
		YamlConfiguration ac = YamlConfiguration.loadConfiguration(af);
		
		return ac.getInt(arena + ".MaxPlayers");
	}
	
	public void beginTimeLockTimers()
	{
		final File af = new File(plugin.getDataFolder() + File.separator + "arenas.yml");
		final YamlConfiguration ac = YamlConfiguration.loadConfiguration(af);

		new BukkitRunnable() {
    		public void run() {
    			for (String name : ac.getKeys(false))
    			{
    				if (ac.getBoolean(name + ".LockedTime.Enabled"))
					{
						int time = ac.getInt(name + ".LockedTime.Time");
    				
						if (time > 18000)
						{
							time = 18000;
							plugin.util.log(LogType.Warning, "[TimeLock] Specified time for arena " + name + " is too high! Using 18000...");
						}
    					
						if (plugin.arena.getSpawn(Team.Red, name) == null)
						{
							plugin.util.log(LogType.Warning, "[TimeLock] No red spawn set? (World Invalid, Arena " + name + ")");
    					
							if (plugin.arena.getSpawn(Team.Blue, name) == null)
							{
		    					plugin.util.log(LogType.Warning, "[TimeLock] No blue spawn set? (World Invalid, Arena " + name + ")");
		    					plugin.util.log(LogType.Warning, "[TimeLock] Red and Blue spawn worlds are invalid, unable to change time for " + name);
		    				} else {
		    					plugin.util.log(LogType.Debug, "[TimeLock] Time set to " + time + " in " + name);
		    					plugin.arena.getSpawn(Team.Blue, name).getWorld().setTime(time);
		    				}
						} else {
							plugin.util.log(LogType.Debug, "[TimeLock] Time set to " + time + " in " + name);
							plugin.arena.getSpawn(Team.Red, name).getWorld().setTime(time);
						}
					}
    			}
    		}
		}.runTaskTimer(plugin, plugin.config.TimeLockInterval * 20, plugin.config.TimeLockInterval * 20);
	}

	public void toBreak(Player p, Team team, String arena) {
		if (toBreak.containsKey(p.getName()))
		{
			toBreak.remove(p.getName());
			toBreakArena.remove(p.getName());
			p.sendMessage(plugin.util.c(plugin.util.MessagePrefix + "&bRequest canceled."));
		} else {
			toBreak.put(p.getName(), team);
			toBreakArena.put(p.getName(), arena);
			p.sendMessage("");
			p.sendMessage(plugin.util.c(plugin.util.MessagePrefix + "&eTo set the target block, just break the target block."));
			p.sendMessage(plugin.util.c(plugin.util.MessagePrefix + "&cTo cancel, just run the command again."));
		}
	}

	public void setTarget(Team team, Block block, Player p, String arena) {
		final File af = new File(plugin.getDataFolder() + File.separator + "arenas.yml");
		final YamlConfiguration ac = YamlConfiguration.loadConfiguration(af);

		if (team == Team.Blue)
		{
			p.sendMessage(plugin.util.c(plugin.util.MessagePrefix + "&9Blue's &eflag block has been updated for arena &6" + arena + "&e."));
			block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, 8);
			block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, 22);
			
			try {
				ac.set(arena + ".TargetBlock.Blue.x", (int) block.getLocation().getX());
				ac.set(arena + ".TargetBlock.Blue.y", (int) block.getLocation().getY());
				ac.set(arena + ".TargetBlock.Blue.z", (int) block.getLocation().getZ());
				ac.set(arena + ".TargetBlock.Blue.pitch", (int) block.getLocation().getYaw());
				ac.set(arena + ".TargetBlock.Blue.yaw", (int) block.getLocation().getPitch());
				ac.set(arena + ".TargetBlock.Blue.world", block.getLocation().getWorld().getName());ac.save(af);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (team == Team.Red)
		{
			p.sendMessage(plugin.util.c(plugin.util.MessagePrefix + "&cRed's &eflag block has been updated for arena &6" + arena + "&e."));
			block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, 10);
			block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, 152);

			try {
				ac.set(arena + ".TargetBlock.Red.x", (int) block.getLocation().getX());
				ac.set(arena + ".TargetBlock.Red.y", (int) block.getLocation().getY());
				ac.set(arena + ".TargetBlock.Red.z", (int) block.getLocation().getZ());
				ac.set(arena + ".TargetBlock.Red.pitch", (int) block.getLocation().getYaw());
				ac.set(arena + ".TargetBlock.Red.yaw", (int) block.getLocation().getPitch());
				ac.set(arena + ".TargetBlock.Red.world", block.getLocation().getWorld().getName());ac.save(af);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
	}
}
