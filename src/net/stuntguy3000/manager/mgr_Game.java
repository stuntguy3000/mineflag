package net.stuntguy3000.manager;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

import net.stuntguy3000.MFPlugin;

public class mgr_Game {
	
	public MFPlugin plugin;
	
	public mgr_Game (MFPlugin instance)
	{
		this.plugin = instance;
	}
	
	public HashMap<String, String> ingame = new HashMap<String, String>();
	public boolean isInGame(Player p) {
		return ingame.containsKey(p.getName());
	}
	
	public String getArenaPlayerIsIn(Player p) {
		return ingame.get(p.getName());
	}
	
	public int getPlayerCount(String arena)
	{
		int value = 0;
		
		for (Map.Entry<String, String> entry : ingame.entrySet()) {
		    String arenaName = entry.getValue();
		    
		    if (arena.equals(arenaName))
		    	value ++;
		}
		
		return value;
	}

	public void join(Player p, String arena) {
		if (ingame.containsKey(p.getName()))
		{
			p.sendMessage(plugin.util.c(plugin.util.MessagePrefix + "&cError joining arena &4" + arena));
			return;
		}
		
		ingame.put(p.getName(), arena);
		
		p.sendMessage(plugin.util.c(""));
		p.sendMessage(plugin.util.c(plugin.util.MessagePrefix + "&eYou have joined &6" + arena + "&e!"));
		
	}
	
	public void leave(Player p)
	{
		if (!ingame.containsKey(p.getName()))
		{
			p.sendMessage(plugin.util.c(plugin.util.MessagePrefix + "&cYou are not in any games!"));
			return;
		}
		
		String arena = ingame.get(p.getName());
		
		p.sendMessage(plugin.util.c(plugin.util.MessagePrefix + "&eYou have left &6" + arena + "&e!"));
		
		ingame.remove(p.getName());
	}
}
