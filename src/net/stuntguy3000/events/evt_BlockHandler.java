package net.stuntguy3000.events;

import net.stuntguy3000.MFPlugin;
import net.stuntguy3000.enums.Team;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class evt_BlockHandler implements Listener {
	public MFPlugin plugin;
	
	public evt_BlockHandler (MFPlugin instance)
	{
		this.plugin = instance;
	}
	
	@EventHandler
	public void BlockBreakEvent(BlockBreakEvent event) {
		
		Player p = event.getPlayer();
		
		if (plugin.arena.toBreak.containsKey(p.getName()))
		{
			Team t = plugin.arena.toBreak.get(p.getName());
			String a = plugin.arena.toBreakArena.get(p.getName());
			
			plugin.arena.setTarget(t, event.getBlock(), p, a);
		
			plugin.arena.toBreak.remove(p.getName());
			plugin.arena.toBreakArena.remove(p.getName());
			event.setCancelled(true);
		}
	}
	
}
