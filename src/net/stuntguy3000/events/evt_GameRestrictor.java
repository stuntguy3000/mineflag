package net.stuntguy3000.events;

import net.stuntguy3000.MFPlugin;
import net.stuntguy3000.enums.LogType;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class evt_GameRestrictor implements Listener {
	
	public MFPlugin plugin;
	
	public evt_GameRestrictor (MFPlugin instance)
	{
		this.plugin = instance;
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
		Player p = event.getPlayer();
		
		String[] msg = event.getMessage().split(" ");
		String firstarg = msg[0].toLowerCase();
		
		
	
		if (firstarg.equalsIgnoreCase("/mf") || firstarg.equalsIgnoreCase("/mineflag"))
		{
			plugin.util.log(LogType.Severe, firstarg);
			
			if (plugin.game.isInGame(p))
			{
				p.sendMessage(plugin.util.c(plugin.util.MessagePrefix + "&cYou cannot use this command during the game!"));
				event.setCancelled(true);
			}
		}
		
	}

}
