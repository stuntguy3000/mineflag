MineFlag
========

MineFlag - A CTF Plugin to be excited about.
MineFlag is a personal project which is (hopefully) going to put a twist on conventional CTF plugins. 
Right now this project is in very early development and planning, so stay tuned.

Features
-	MySQL/Flatfile Stats
-	Multiple Games/Arenas
-	Planned Easy to use API
-	Teams or Deathmatch
-	Limited or Unlimited Lives (Extensive Control over the game)
-	Fully permission based

Plugin Description

MineFlag is a Minecraft CTF plugin made for Bukkit! Play with your friends and capture the flag before the opposing team does! Battle through the field and grab that flag and get it to your base to win! MineFlag is based by the server admin for the server admin! Were all about simplicity when it comes to making plugins! Simply type "/MF" To bring up the diolag for the plugin! Then type "/MF Help" for information about how to set your arenas for your MineFlag Server! Get MineFlag today and keep the hassle away!

Bukkit-Dev Link
http://dev.bukkit.org/bukkit-plugins/mineflag/